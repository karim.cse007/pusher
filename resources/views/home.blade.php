@extends('layouts.chat.app')

@section('content')
    <Chat :user="{{auth()->user()}}"></Chat>
@endsection
