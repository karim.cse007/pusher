<?php

namespace App\Http\Controllers;

use App\Events\MessageEvent;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public function getContacts()
    {
        $users = User::where('id','!=',auth()->id())->get();
        $unreadIds = Message::select(DB::raw('`from` as sender_id, count(`from`) as messages_count'))
            ->where('to', auth()->id())
            ->where('is_read', false)
            ->groupBy('from')
            ->get();
        $users = $users->map(function($user) use ($unreadIds) {
            $contactUnread = $unreadIds->where('sender_id', $user->id)->first();

            $user->unread = $contactUnread ? $contactUnread->messages_count : 0;

            return $user;
        });

        return response()->json($users,200);
    }
    public function getMessage($id)
    {
        // mark all messages with the selected contact as read
        Message::where('from', $id)->where('to', auth()->id())->update(['is_read' => true]);

        $message = Message::where(function ($q) use ($id) {
            $q->where('from',auth()->id());
            $q->where('to',$id);
        })->orWhere(function ($q) use ($id) {
            $q->where('from',$id);
            $q->where('to',auth()->id());
        })->get();
        return response()->json($message,200);
    }
    public function sentMessage(Request $request)
    {
        $message = new Message();
        $message->from = auth()->id();
        $message->to = $request->contact_id;
        $message->message = $request->message;
        $message->save();
        broadcast(new MessageEvent($message->load('fromContact')));
        return response()->json($message,200);
    }
}
